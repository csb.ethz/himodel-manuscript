#!/usr/bin/env Rscript
# This file is part of the following manuscript:
# Title:   Model-based inference of neutralizing antibody avidities against 
#          influenza virus
# Authors: Janina Linnik, Mohammedyaseen Syedbasha, Yvonne Hollenstein,
#          Jörg Halter, Adrian Egli*, Jörg Stelling*
# *Corresponding authors
#
# (C) Copyright 2020-2022 ETH Zurich, D-BSSE, Janina Linnik
#
# Licensed under the MIT license:
#
#     http://www.opensource.org/licenses/mit-license.php

## Obtain upper and lower credibility interval for inferred avidities and 
## classify patient-response

library(dplyr)
library(tidyr)
library(here)

# Import data and results
meta_data <- read.csv(
  file.path(here::here(), "data", "data_processed.csv")
) 

chains <- read.csv(
  file = file.path(
    here::here(), "results", "posteriors", "chains_avidity_burned.csv")
)

import_results <- function() {
  results_path <- file.path(here::here(), "results", "posteriors")
  files <- list.files(
    results_path,
    full.names = T, 
    pattern = "sampled_titers")
  
  results <- purrr::map_df(
    files, ~ read.csv(.))
  
  results
}
sampling_results <- import_results() %>%
  dplyr::select(ID, time, titer_diff, percent, n_samples, seed)

# Get median and lower/upper interval bounds for avidity
get_mid <- function(data){median(data$K_d_app)}
get_lower <- function(data){quantile(data$K_d_app, probs = unique(data$q))}
get_upper <- function(data){quantile(data$K_d_app, probs = 1 - unique(data$q))}
summary <- chains %>%
  dplyr::select(ID, time, K_d_app) %>%
  dplyr::right_join(sampling_results, by = c("ID", "time")) %>%
  dplyr::filter(titer_diff == 0) %>%
  dplyr::select(-titer_diff) %>%
  dplyr::mutate(q = percent / 2) %>%
  tidyr::nest(data = -c("ID", "time")) %>%
  dplyr::mutate(
    K_d_app_mid = lapply(data, get_mid),
    K_d_app_lower = lapply(data, get_lower),
    K_d_app_upper = lapply(data, get_upper)
  ) %>%
  tidyr::unnest(cols = c(data, K_d_app_mid, K_d_app_lower, K_d_app_upper)) %>%
  dplyr::select(-c("K_d_app")) %>%
  dplyr::distinct() %>%
  dplyr::mutate(
    rel_error = 0.5 * (K_d_app_upper - K_d_app_lower) / K_d_app_mid
  ) %>%
  # Add patient data information
  dplyr::left_join(meta_data, by = c("ID", "time")) %>%
  # Minimal inhibitory IgG concentration
  dplyr::mutate(
    igg_at_min_inh_dilution = igg_est / titer
  ) %>% 
  # Check samples below/above resolution limit
  dplyr::mutate(
    lower_K_d_bound = ifelse(K_d_app_lower < 10^(-1.5), 1, 0)
  ) %>%
  dplyr::mutate(lower_titer_bound = ifelse(titer < 8, 1, 0))

# Process values of titers below HI assay resolution
summary_lower_titer_bound <- chains %>%
  dplyr::select(ID, time, K_d_app) %>%
  dplyr::right_join(sampling_results, by = c("ID", "time")) %>%
  dplyr::filter(titer_diff == 1) %>% # random col
  dplyr::select(-titer_diff) %>%
  dplyr::mutate(q = .27) %>%
  tidyr::nest(data = -c("ID", "time")) %>%
  dplyr::mutate(
    K_d_app_mid = lapply(data, get_mid),
    K_d_app_lower = lapply(data, get_lower),
    K_d_app_upper = lapply(data, get_upper)
  ) %>%
  tidyr::unnest(cols = c(data, K_d_app_mid, K_d_app_lower, K_d_app_upper)) %>%
  dplyr::select(-c("K_d_app")) %>%
  dplyr::distinct() %>%
  dplyr::mutate(
    rel_error = 0.5 * (K_d_app_upper - K_d_app_lower) / K_d_app_mid
  ) %>%
  dplyr::left_join(meta_data, by = c("ID", "time")) %>%
  dplyr::mutate(
    igg_at_min_inh_dilution = igg_est / titer
  ) %>% 
  dplyr::mutate(
    lower_K_d_bound = 0
  ) %>%
  dplyr::mutate(lower_titer_bound = ifelse(titer < 8, 1, 0)) %>%
  dplyr::filter(lower_titer_bound == 1) %>%
  dplyr::group_by(ID) %>%
  dplyr::mutate(
    K_d_app_mid = max(K_d_app_mid),
    K_d_app_lower = max(K_d_app_lower),
    K_d_app_upper = max(K_d_app_upper)
  ) %>%
  dplyr::ungroup()

# Process values of avidities below HI assay resolution
summary_lower_K_d_bound <- summary %>%
  dplyr::filter(lower_K_d_bound == 1) %>%
  dplyr::mutate(
    K_d_app_mid = 10^(-1.5), 
    K_d_app_lower = 10^(-1.5) - 0.27*10^(-1.5),
    K_d_app_upper = 10^(-1.5) + 0.27*10^(-1.5)
  ) 

# Combine all results
summary_processed <- summary %>%
  dplyr::filter(lower_titer_bound == 0) %>%
  dplyr::filter(lower_K_d_bound == 0) %>%
  dplyr::bind_rows(summary_lower_titer_bound) %>%
  dplyr::bind_rows(summary_lower_K_d_bound)

# Compute fold change compared to d0 (or d7 if d0 is missing)
eps = 0.6
K_d_d0 <- summary_processed %>%
  dplyr::filter(time == 0) %>%
  dplyr::mutate(
    K_d_app_mid_d0 = ifelse(rel_error < eps, K_d_app_mid, NA),
    K_d_app_lower_d0 = ifelse(rel_error < eps, K_d_app_lower, NA),
    K_d_app_upper_d0 = ifelse(rel_error < eps, K_d_app_upper, NA)
  ) %>%
  dplyr::select(ID, K_d_app_mid_d0, K_d_app_lower_d0, K_d_app_upper_d0)
# Compute fold change compared to day 7
K_d_d7 <- summary_processed %>%
  dplyr::filter(time == 7) %>%
  dplyr::mutate(
    K_d_app_mid_d7 = ifelse(rel_error < eps, K_d_app_mid, NA),
    K_d_app_lower_d7 = ifelse(rel_error < eps, K_d_app_lower, NA),
    K_d_app_upper_d7 = ifelse(rel_error < eps, K_d_app_upper, NA)
  ) %>%
  dplyr::select(ID, K_d_app_mid_d7, K_d_app_lower_d7, K_d_app_upper_d7)

fold_changes <- summary_processed %>%
  dplyr::left_join(K_d_d0, by = "ID") %>%
  dplyr::left_join(K_d_d7, by = "ID") %>%
  dplyr::filter(rel_error < eps) %>% # Error >60% due to ELISA measurement error
  dplyr::filter(concentration.est > 0.002) %>% # IgG signal below blank
  dplyr::mutate(
    missing_baseline = ifelse(
      is.na(K_d_app_mid_d0), 1, 0
      )
  ) %>%
  dplyr::mutate(
    K_d_app_fc = ifelse(
      time != 0 | missing_baseline == 0, K_d_app_mid / K_d_app_mid_d0, NA),
    K_d_app_fc_lower = ifelse(
      time != 0 | missing_baseline == 0, K_d_app_lower / K_d_app_upper_d0, NA),
    K_d_app_fc_upper = ifelse(
      time != 0 | missing_baseline == 0, K_d_app_upper / K_d_app_lower_d0, NA)
  ) %>%
  dplyr::mutate(
    K_d_app_fc = ifelse(
      time > 7 & missing_baseline == 1, 
      K_d_app_mid / K_d_app_mid_d7, K_d_app_fc
      ),
    K_d_app_fc_lower = ifelse(
      time > 7 & missing_baseline == 1, 
      K_d_app_lower / K_d_app_upper_d7, K_d_app_fc_lower
      ),
    K_d_app_fc_upper = ifelse(
      time > 7 & missing_baseline == 1, 
      K_d_app_upper / K_d_app_lower_d7, K_d_app_fc_upper
      )
  ) %>%
  # Change in avidity significant?
  dplyr::mutate(
    signif_avidity_increase = ifelse(K_d_app_fc_upper < 1, 1, 0),
    signif_avidity_decrease = ifelse(K_d_app_fc_lower > 1, 1, 0)
  )

# Identify patients with nonneutralizing IgG response
nonneutralizing_response <- fold_changes %>%
  dplyr::filter(titer_fc <= 1) %>%
  dplyr::filter(igg_fc > 1) %>%
  dplyr::group_by(ID) %>%
  dplyr::filter(time != 180) %>%
  dplyr::summarise(count_decrease = sum(signif_avidity_decrease, na.rm = T)) %>%
  dplyr::filter(count_decrease > 0)
length(unique(nonneutralizing_response$ID))

# Identify patients with increase in avidity
avidity_increase <- fold_changes %>%
  dplyr::filter(titer_fc > 1) %>%
  dplyr::filter(signif_avidity_increase == 1) 
length(unique(avidity_increase$ID))

# Identify potential GC candidates
gc_candidate <- fold_changes %>%
  dplyr::filter(titer_fc > 1) %>%
  dplyr::filter(signif_avidity_increase == 1) %>%
  dplyr::filter(time == 30 | time == 60)  
length(unique(gc_candidate$ID))

# Summarise and save results
out <- fold_changes %>%
  dplyr::mutate(
    not_classifiable = ifelse(
      is.na(K_d_app_mid_d0) & is.na(K_d_app_mid_d7), 1, 0
    )
  ) %>%
  dplyr::mutate(
    nonneutralizing = ifelse(ID %in% nonneutralizing_response$ID, 1, 0),
    avidity_increase = ifelse(ID %in% avidity_increase$ID, 1, 0),
    gc_candidate = ifelse(ID %in% gc_candidate$ID, 1, 0)
  )  
write.csv(
  out, 
  file = file.path(
    here::here(), "results", "inferred_avidities.csv"),
  row.names = F
)
