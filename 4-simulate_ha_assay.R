#!/usr/bin/env Rscript
# This file is part of the following manuscript:
# Title:   Model-based inference of neutralizing antibody avidities against 
#          influenza virus
# Authors: Janina Linnik, Mohammedyaseen Syedbasha, Yvonne Hollenstein,
#          Jörg Halter, Adrian Egli*, Jörg Stelling*
# *Corresponding authors
#
# (C) Copyright 2020-2022 ETH Zurich, D-BSSE, Janina Linnik
#
# Licensed under the MIT license:
#
#     http://www.opensource.org/licenses/mit-license.php

library(dplyr)
library(tidyr)
library(readr)
library(deSolve)
library(purrr)
library(here)
library(tictoc)
library(himodel)

sample_input <- function(n) {
  
  sample_b_star <- function(n){
    b_star <- function(RBC_area, b, virus_area) {
      b_star <- virus_area / (RBC_area / b)
    }
    RBC_area = runif(n, 130, 170)*10^6
    b = runif(n, 0.4, 0.5)*10^6
    virus_area = (60)^2 * pi
    out <- b_star(RBC_area, b, virus_area)
    out
  }
  
  rbc_total = 3.114e-05
  r = sample(300:500, size = n, replace = TRUE)
  k_ass_rbc = rnorm(n, 2e-6, sd = 0.2*2e-6) 
  k_diss_rbc = rnorm(n, 2e-4, sd = 0.2*2e-4)
  b = runif(n, 0.4, 0.5)*10^6
  b_star = sample_b_star(n)
  
  X <- cbind(
    rbc_total, r, k_ass_rbc, k_diss_rbc, b, b_star
  )
  X
}

run_ha_assay <- function(virus, t, n){
  set.seed(42)
  X <- sample_input(n)
  out <- purrr::map_df(
    1:nrow(X),
    ~ simulate_hemagglutination(
      ab_dilution_conc = 0, # Set to 0 nM to simulate HA assay
      K_d_app = 1, 
      virus_conc = virus,
      t_readout = t,
      rbc_conc = X[[.,1]],
      r = X[[.,2]],
      k_ass_rbc = X[[.,3]],
      k_diss_rbc = X[[.,4]],
      b = X[[.,5]],
      b_star = X[[.,6]]
    )
  )
 out
}

# Define virus dilutions
virus_total = (2^seq(-8,8,by=1)) * 0.00013
# Define time range (1 second to 45 min)
t_readout = c(seq(1,911,10), seq(971,2701,60))

# Simulate hemagglutination for all time points and virus dilutions
vars <- expand.grid(virus_total = virus_total, t_readout = t_readout)

# Set sample size
n = 1000

# Perform HA titration assays
ha_assay_results <- purrr::map_df(
  1:nrow(vars),
  ~ run_ha_assay(vars$virus_total[.], vars$t_readout[.], n)
)

# Save results
write.csv(
  ha_assay_results,
  file = file.path(
    here::here(), "results", "simulation_studies", "ha_assay.csv"
  ),
  row.names = F
)

