[![License: MIT](https://img.shields.io/badge/License-MIT-grey.svg)](https://opensource.org/licenses/MIT)
## Information

This repository contains data and analysis scripts related to the following manuscript:

Janina Linnik, Mohammedyaseen Syedbasha, Yvonne Hollenstein, Jörg Halter, Adrian Egli*, Jörg Stelling*. __Model-based inference of neutralizing antibody avidities against influenza virus__, 2022. _Accepted for publication in PLOS Pathogens_.

*Corresponding authors:
Jörg Stelling <joerg.stelling@bsse.ethz.ch>,
Adrian Egli <adrian.egli@usb.ch>

## Content

* `data`: ELISA and HI assay measurements.
* `results`: Inferred avidities, model simulations, and statistical analysis results.
* `paper-figures`: Figure-generating R scripts.
* Analysis pipeline (see numbered R scripts).

## Requirements

* R version >= 3.6
* himodel (0.9) (https://gitlab.com/csb.ethz/himodel)
* checkpoint (0.4.9)

## Author

* Janina Linnik <janina.linnik@bsse.ethz.ch>

## Licence

Licensed under the MIT license: http://www.opensource.org/licenses/mit-license.php
